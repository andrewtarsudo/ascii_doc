from collections import Counter
from re import Pattern, compile, search, Match

from functions import StrNone


class ReplaceBaseError(Exception):
    """Base error class for Replace elements."""


class ReplaceFromPatternError(ReplaceBaseError):
    """Invalid from_pattern."""


class ReplaceToPatternError(ReplaceBaseError):
    """Invalid to_pattern."""


class ReplacePattern:
    def __init__(self, from_pattern: str, to_pattern: str):
        self._from: str = from_pattern
        self._to: str = to_pattern
        self._validate()

    @property
    def _counter_from(self):
        return Counter(self._from)

    @property
    def _counter_to(self):
        return Counter(self._to)

    def _validate(self):
        _from: bool = self._counter_from.get("(") == self._counter_from.get(")")
        _to: bool = self._counter_to.get("(") == self._counter_to.get(")")
        if not _from:
            raise ReplaceFromPatternError
        if not _to:
            raise ReplaceToPatternError

    def __len__(self):
        return len(self.match_groups())

    @property
    def from_pattern(self):
        return compile(self._from)

    @property
    def to_pattern(self):
        return compile(self._to)

    def _match_to(self) -> Match | None:
        pattern: Pattern = compile(r"\\\d+")
        return search(pattern, self._to)

    def match_groups(self):
        if self._match_to():
            return self._match_to().groups()
        else:
            return tuple()


class Replacer:
    def __init__(self, line: StrNone):
        self._line: str = line

    @property
    def line(self):
        return self._line

    @line.setter
    def line(self, value):
        if self._line is not None:
            self.nullify()
        self._line = value

    def nullify(self):
        self._line = None

