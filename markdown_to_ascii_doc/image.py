from conversion import ConversionElement
from functions import StrNone
from re import Pattern, compile, Match, fullmatch

from errors import LineError


class Image(ConversionElement):
    def __init__(self, link: str, text: StrNone, **kwargs):
        super().__init__(**kwargs)
        if text is None:
            text: str = ""
        self._link: str = link
        self._text: str = text

    def as_markdown(self):
        return f"![{self._text}]({self._link})"

    def as_ascii_doc(self):
        return f"image::{self._link}[{self._text}]"

    @classmethod
    def from_markdown(cls, line: str):
        pattern: Pattern = compile(r"!\[(.*)]\((.*)\)")
        _m: Match = fullmatch(pattern, line)
        if not _m:
            raise LineError
        text: str = _m.group(1)
        link: str = _m.group(2)
        return cls(link, text)

    @classmethod
    def from_ascii_doc(cls, line: str):
        pattern: Pattern = compile(r"image::(.*)\[(.*)]")
        _m: Match = fullmatch(pattern, line)
        if not _m:
            raise LineError
        text: str = _m.group(2)
        link: str = _m.group(1)
        return cls(link, text)
