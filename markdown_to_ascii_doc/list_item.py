from re import Match, Pattern, compile, fullmatch

from conversion import ConversionElement
from errors import LineError


class ListItem(ConversionElement):
    def __init__(self, level: int, text: str, **kwargs):
        super().__init__(**kwargs)
        self._level: int = level
        self._text: str = text

    def as_markdown(self):
        _space: str = 2 * (self._level - 1) * " "
        return f"{_space}* {self._text}"

    def as_ascii_doc(self):
        _marks: str = self._level * "*"
        return f"{_marks} {self._text}"

    @classmethod
    def from_markdown(cls, line: str):
        pattern: Pattern = compile(r"(\s*)[*\-](.*)")
        _m: Match = fullmatch(pattern, line)
        if not _m:
            raise LineError
        level: int = len(_m.group(1)) // 2
        text: str = _m.group(2)
        return cls(level, text)

    @classmethod
    def from_ascii_doc(cls, line: str):
        pattern: Pattern = compile(r"(\**)\s(.*)")
        _m: Match = fullmatch(pattern, line)
        if not _m:
            raise LineError
        level: int = len(_m.group(1))
        text: str = _m.group(2)
        return cls(level, text)
