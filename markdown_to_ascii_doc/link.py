from re import Match, Pattern, compile, fullmatch

from conversion import ConversionElement
from errors import LineError


class InternalLink(ConversionElement):
    def __init__(self, link: str, text: str, **kwargs):
        super().__init__(**kwargs)
        self._link: str = link
        self._text: str = text

    def as_markdown(self):
        return f"[{self._text}](#{self._link})"

    def as_ascii_doc(self):
        return f"<<{self._text},{self._link}>>"

    @classmethod
    def from_markdown(cls, line: str):
        pattern: Pattern = compile(r"\[(.*)]\(#(.*)\)")
        _m: Match = fullmatch(pattern, line)
        if not _m:
            raise LineError
        text: str = _m.group(1)
        link: str = _m.group(2)
        return cls(link, text)

    @classmethod
    def from_ascii_doc(cls, line: str):
        pattern: Pattern = compile(r"<<(.*),(.*)>>")
        _m: Match = fullmatch(pattern, line)
        if not _m:
            raise LineError
        text: str = _m.group(1)
        link: str = _m.group(2)
        return cls(link, text)


class ExternalLink(ConversionElement):
    def __init__(self, path: str, link: str, text: str, **kwargs):
        super().__init__(**kwargs)
        self._path: str = path
        self._link: str = link
        self._text: str = text

    def as_markdown(self):
        return f"[{self._text}]({self._path}#{self._link})"

    def as_ascii_doc(self):
        return f"xref:{self._path}#{self._link}[{self._text}]"

    @classmethod
    def from_markdown(cls, line: str):
        pattern: Pattern = compile(r"\[(.*)]\((.*)#(.*)\)")
        _m: Match = fullmatch(pattern, line)
        if not _m:
            raise LineError
        text: str = _m.group(1)
        path: str = _m.group(2)
        link: str = _m.group(3)
        return cls(link, path, text)

    @classmethod
    def from_ascii_doc(cls, line: str):
        pattern: Pattern = compile(r"xref:(.*)#(.*)\[(.*)]")
        _m: Match = fullmatch(pattern, line)
        if not _m:
            raise LineError
        path: str = _m.group(1)
        link: str = _m.group(2)
        text: str = _m.group(3)
        return cls(path, link, text)


class UrlLink(ConversionElement):
    def __init__(self, link: str, text: str, **kwargs):
        super().__init__(**kwargs)
        self._link: str = link
        self._text: str = text

    def as_markdown(self):
        return f"[{self._text}]({self._link})"

    def as_ascii_doc(self):
        return f"{self._text}\[{self._link}]"

    @classmethod
    def from_markdown(cls, line: str):
        pattern: Pattern = compile(r"\[(.*)]\((.*)\)")
        _m: Match = fullmatch(pattern, line)
        if not _m:
            raise LineError
        text: str = _m.group(1)
        link: str = _m.group(2)
        return cls(link, text)

    @classmethod
    def from_ascii_doc(cls, line: str):
        pattern: Pattern = compile(r"(.*)\[(.*)]")
        _m: Match = fullmatch(pattern, line)
        if not _m:
            raise LineError
        text: str = _m.group(1)
        link: str = _m.group(2)
        return cls(link, text)
