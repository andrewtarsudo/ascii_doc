from typing import Any


class ConversionElement:
    def __init__(self, **kwargs):
        if kwargs is None:
            kwargs: dict[str, Any] = dict()
        self._args: list[str] = [*kwargs.keys()]
        for k, v in kwargs.items():
            setattr(self, k, v)

    def as_markdown(self):
        raise NotImplementedError

    def as_ascii_doc(self):
        raise NotImplementedError

    @classmethod
    def from_markdown(cls, line: str):
        raise NotImplementedError

    @classmethod
    def from_ascii_doc(cls, line: str):
        raise NotImplementedError
