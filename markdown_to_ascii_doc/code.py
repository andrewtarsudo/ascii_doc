from re import Match, Pattern, compile, fullmatch

from conversion import ConversionElement
from errors import LineError


class Code(ConversionElement):
    def __init__(self, code: str, **kwargs):
        super().__init__(**kwargs)
        self._code: str = code

    def as_markdown(self):
        return f"```\n{self._code}\n```"

    def as_ascii_doc(self):
        return f"----\n{self._code}\n----"

    @classmethod
    def from_markdown(cls, line: str):
        pattern: Pattern = compile(r"```\n(.*)\n```")
        _m: Match = fullmatch(pattern, line)
        if not _m:
            raise LineError
        code: str = _m.group(1)
        return cls(code)

    @classmethod
    def from_ascii_doc(cls, line: str):
        pattern: Pattern = compile(r"----\n(.*)\n----")
        _m: Match = fullmatch(pattern, line)
        if not _m:
            raise LineError
        code: str = _m.group(1)
        return cls(code)
