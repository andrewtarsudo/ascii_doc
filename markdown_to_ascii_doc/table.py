from itertools import pairwise
from typing import Iterable, Sequence

from conversion import ConversionElement


def bound(line: str, border: str):
    return f"{border}{line}{''.join(reversed(border))}"


def separate(lines: Iterable[str], separator: str):
    return separator.join(lines)


def continuous_indexes(indexes: Sequence[int] = None):
    if indexes is None:
        indexes: Sequence[int] = [0]
    _stops: list[int] = [0]
    _diff: int = indexes[0]
    for index, value in enumerate(indexes):
        if value - index != _diff:
            _stops.append(index)
            _diff: int = value - index
    _stops.append(len(indexes))
    return [indexes[_from:_to] for _from, _to in pairwise(_stops)]


def get_non_empty_from_to(boundary: str, lines: Iterable[str]):
    _indexes: list[int] = [
        index for index, line in enumerate(lines)
        if line.removesuffix("\n") == boundary]
    _index_from, _index_to = _indexes[0], _indexes[1]
    return [item for item in lines[_index_from + 1:_index_to] if item]


class Table(ConversionElement):
    def __init__(self, heading: Iterable[str], lines: Iterable[Iterable[str]], **kwargs):
        super().__init__(**kwargs)
        self._heading: list[str] = [*heading]
        self._lines: list[list[str]] = [*lines]

    def __len__(self):
        return len(self._heading)

    def as_markdown(self):
        _heading: str = bound(separate(self._heading, " | "), "| ")
        _subheader: str = bound(separate(["---"] * len(self), "|"), "|")
        _lines: list[str] = [bound(separate(line, " | "), "| ") for line in self._lines]
        return "\n".join((_heading, _subheader, *_lines))

    def as_ascii_doc(self):
        _table_mark: str = "|==="
        _heading: str = " ".join([f"|{_head}" for _head in self._heading])
        _lines: list[str] = [" ".join([f"|{_line}" for _line in self._lines])]
        return "\n".join((_table_mark, "\n", _heading, *_lines, "\n", _table_mark))

    @classmethod
    def from_markdown(cls, line: str):
        _heading: list[str] = []
        _lines: list[list[str]] = []
        lines: list[str] = line.split("\n")
        _indexes: list[int] = [
            index for index, _line in enumerate(lines)
            if _line.startswith("|") and _line.endswith("|")]
        _table_list: list[int] = [*continuous_indexes(_indexes)[0]]
        _heading_line: str = lines[_table_list[0]].removesuffix("\n")
        _heading: list[str] = [_.strip() for _ in _heading_line[1:-1].split("|")]
        _lines: list[list[str]] = [
            [_.strip() for _ in lines[_line_index].removesuffix("\n")[1:-1].split("|")]
            for _line_index in _table_list[2:]
        ]

        return cls(_heading, _lines)

    @classmethod
    def from_ascii_doc(cls, line: str):
        _heading: list[str] = []
        _lines: list[list[str]] = []
        lines: list[str] = get_non_empty_from_to("|===", line.split("\n"))
        _heading: list[str] = [_.strip() for _ in lines[0].split("|")[1:]]
        _lines: list[list[str]] = [
            [_line.strip() for _line in _line.split("|")[1:]]
            for _line in lines[1:]
        ]
        return cls(_heading, _lines)
