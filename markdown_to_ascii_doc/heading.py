from re import Pattern, Match, fullmatch, compile

from conversion import ConversionElement
from errors import LineError


class Heading(ConversionElement):
    def __init__(self, level: int, text: str, **kwargs):
        super().__init__(**kwargs)
        self._level: int = level
        self._text: str = text

    def as_markdown(self):
        _octotorps: str = self._level * "#"
        return f"{_octotorps} {self._text}"

    def as_ascii_doc(self):
        _equal_signs: str = self._level * "#"
        return f"{_equal_signs} {self._text}"

    @classmethod
    def from_markdown(cls, line: str):
        pattern: Pattern = compile(r"(#*)\s(.*)\s?(#*)")
        _m: Match = fullmatch(pattern, line)
        if not _m:
            raise LineError
        level: int = len(_m.group(1))
        text: str = _m.group(2)
        return cls(level, text)

    @classmethod
    def from_ascii_doc(cls, line: str):
        pattern: Pattern = compile(r"(=*)\s(.*)")
        _m: Match = fullmatch(pattern, line)
        if not _m:
            raise LineError
        level: int = len(_m.group(1))
        text: str = _m.group(2)
        return cls(level, text)
