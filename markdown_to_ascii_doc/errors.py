class LineError(ValueError):
    """Line does not correspond to the class."""
