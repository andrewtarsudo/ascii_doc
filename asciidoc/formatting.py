from enum import Enum
from typing import Iterable, NamedTuple


class FormattingType(NamedTuple):
    style_name: str
    label: str
    is_basic: bool

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.style_name}, {self.label})>"

    def __str__(self):
        return f"{self.style_name}: {self.label}__text__{self.label}"


class FormattingStyle(Enum):
    BOLD = FormattingType("bold", "*", False)
    ITALIC = FormattingType("italic", "_", False)


class Formatting:
    def __init__(self, text: Iterable[str] = None):
        if isinstance(text, str):
            text: list[str] = [text]
        if text is None:
            text: list[str] = []
        self._text: list[str] = [*text]
