from typing import Iterable

from loguru import logger

from attribute import Attribute
from base_element import BaseElement
from exceptions import HeadingLevelError, HeadingNameError
from functions import StrNone


class Heading(BaseElement):
    def __bool__(self):
        return True

    def __init__(self, level: int, name: StrNone, attributes: Iterable[Attribute] = None, **kwargs):
        self._level: int = level
        self._name: StrNone = name
        super().__init__(attributes, **kwargs)

    def __str__(self):
        _marker: str = "=" * (self._level + 1)
        return f"{_marker} {self._name}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._level}, {self._name})>"

    def _validate(self):
        if self._name is None:
            logger.error(f"Заголовок не имеет имени")
            raise HeadingNameError
        if self._level <= 0:
            logger.error(f"Некорректный уровень {self._level} заголовка {self._name}")
            raise HeadingLevelError
        return

    @property
    def level(self):
        return self._level

    @level.setter
    def level(self, value):
        self._level = value

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    def uplevel(self, diff: int) -> None:
        if diff == 0:
            return
        if diff < 0:
            return self.downlevel(-diff)
        self._level = max(self._level - diff, 0)
        return

    def downlevel(self, diff: int) -> None:
        if diff == 0:
            return
        if diff < 0:
            return self.uplevel(-diff)
        self._level += diff
        return

    def __hash__(self):
        return hash((self._level, self._name))

    def __key(self):
        return self._level, self._name

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() != other.__key()
        else:
            return NotImplemented
