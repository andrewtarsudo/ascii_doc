from typing import Any, Iterable

from loguru import logger

from attribute import Attribute, AttributeList
from validated import Validated


class BaseElement(Validated):
    def __init__(self, attributes: Iterable[Attribute], **kwargs):
        if attributes is None:
            attributes: list[Attribute] = []
        super().__init__(**kwargs)
        self._attributes: set[Attribute] = {*attributes}

    def _validate(self):
        raise NotImplementedError

    @classmethod
    def from_string(cls, attributes: Iterable[Attribute], **kwargs):
        return cls(attributes, **kwargs)

    def __bool__(self):
        raise NotImplementedError

    @property
    def attribute_list(self):
        return AttributeList(self._attributes)

    def __str__(self):
        return str(self.attribute_list)

    def __repr__(self):
        return f"<{self.__class__.__name__}({repr(self.attribute_list)})>"

    def iter_attributes(self):
        return iter(self.attribute_list)

    def __iter__(self):
        return iter(self.attribute_list)

    def len_attributes(self):
        return len(self.attribute_list)

    def __len__(self):
        return len(self.attribute_list)

    def contains_attributes(self, item):
        if isinstance(item, Attribute):
            return item in self.attribute_list
        elif isinstance(item, str):
            return item in self.attribute_list.attribute_names
        return False

    def __contains__(self, item):
        if isinstance(item, Attribute):
            return item in self.attribute_list
        elif isinstance(item, str):
            return item in self.attribute_list.attribute_names
        return False

    def get_attribute(self, item):
        if not isinstance(item, str):
            return logger.error(f"Ключ {item} должен быть типа str, но получен {type(item)}")
        if item not in self.attribute_list.attribute_names:
            return logger.error(f"Опция {item} не найдена")
        return self.attribute_list[item]

    def __getitem__(self, item):
        if not isinstance(item, str):
            return logger.error(f"Ключ {item} должен быть типа str, но получен {type(item)}")
        if item not in self.attribute_list.attribute_names:
            return logger.error(f"Опция {item} не найдена")
        return self.attribute_list[item]

    def __add__(self, other):
        if not isinstance(other, Attribute):
            logger.error(f"Добавляемый элемент {other} должен быть Attribute, но получен {type(other)}")
            return NotImplemented
        if other in self.attribute_list:
            logger.error(f"Добавляемый атрибут {other.name} уже задан")
            return
        return self.attribute_list + other

    def add_attribute(self, attribute: Attribute):
        return self.attribute_list + attribute

    def add_option(self, name: str, value: Any | None = None):
        if name not in self._args:
            self._args.append(name)
        setattr(self, name, value)
