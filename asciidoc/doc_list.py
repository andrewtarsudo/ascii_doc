from enum import Enum, auto
from typing import Iterable

from loguru import logger

from base_element import BaseElement
from classes import Char
from attribute import Attribute, AttributeList
from exceptions import ListItemInvalidLevelError
from functions import StrNone


class ListType(Enum):
    ORDERED_LIST = auto
    UNORDERED_LIST = auto
    CHECK_LIST = auto
    DESCRIPTION_LIST = auto

    def __str__(self):
        return f"{self.__class__.__name__}({self._name_})"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._name_})>"


class ListItem(BaseElement):
    def __bool__(self):
        return bool(self._text)

    def __init__(
            self,
            level: int,
            list_type: ListType,
            text: StrNone = None,
            attributes: Iterable[Attribute] = None, **kwargs):
        if attributes is None:
            attributes: list[Attribute] = []
        super().__init__(attributes, **kwargs)
        self._level: int = level
        self._list_type: ListType = list_type
        self._text: StrNone = text
        self._attributes: list[Attribute] = [*attributes]

    def _validate(self):
        if self._level <= 0:
            logger.error(f"Уровень должен быть положительным числом, но получен {self._level}")
            raise ListItemInvalidLevelError
        return

    @property
    def attributes_list(self):
        return AttributeList(self._attributes)

    def _start(self):
        raise NotImplementedError

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._level}, {self._list_type}, {self._text}, {self.attributes_list})>"

    __str__ = __repr__

    def __iter__(self):
        return iter(self.attributes_list)


class OrderedListItem(ListItem):
    def __init__(
            self,
            level: int,
            text: StrNone = None,
            attributes: Iterable[Attribute] = None, **kwargs):
        list_type: ListType = ListType.ORDERED_LIST
        _attributes: list[Attribute] = list(filter(lambda x: x.attribute_type.is_element, attributes))
        super().__init__(level, list_type, text, _attributes, **kwargs)

    def _start(self):
        return Char(".") * self._level

    def __str__(self):
        _options: str = ",".join([str(_) for _ in self.attributes_list])
        return f"[{_options}]\n{self._start()} {self._text}"


class UnorderedListItem(ListItem):
    def __init__(
            self,
            level: int,
            text: StrNone = None,
            attributes: Iterable[Attribute] = None, **kwargs):
        list_type: ListType = ListType.UNORDERED_LIST
        _attributes: list[Attribute] = list(filter(lambda x: x.attribute_type.is_element, attributes))
        super().__init__(level, list_type, text, _attributes, **kwargs)

    def _start(self):
        return Char("*") * self._level

    def __str__(self):
        _options: str = ",".join([str(_) for _ in self.attributes_list])
        return f"[{_options}]\n{self._start()} {self._text}"


class DocList:
    def __init__(
            self,
            title: StrNone,
            items: Iterable[ListItem] = None,
            attributes: Iterable[Attribute] = None):
        if items is None:
            items: list[ListItem] = []
        if attributes is None:
            attributes: list[Attribute] = []
        self._title: str = title
        self._items: list[ListItem] = [*items]
        self._attributes: list[Attribute] = [*attributes]
