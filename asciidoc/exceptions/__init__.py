class ItemsEmptyError(ValueError):
    """Iterable set is empty."""


class DocumentLineIndexError(AttributeError):
    """Line index must be a positive integer."""


class DocumentLineKeyTypeError(TypeError):
    """Line key must be of str or int (slice) type."""


class DocumentPartValueTypeError(TypeError):
    """Document part must be of str or AsciiDocPart type."""


class AsciiDocLineInitError(AttributeError):
    """Attribute 'item' must be of str or int type."""


class AsciiDocLineNotFoundError(KeyError):
    """Line is not found."""


class AsciiDocTableNotationError(ValueError):
    """First and last line must be |===."""


class HeadingLevelError(ValueError):
    """Invalid heading level."""


class HeadingNameError(ValueError):
    """Invalid heading name."""


class DeclarationCSSEqualPropertyError(ValueError):
    """Two declarations implement the same object property."""


class ElementCSSNewItemError(TypeError):
    """Declaration must be of the DeclarationCSS type."""


class ElementCSSDeleteItemError(TypeError):
    """Declaration must be of the DeclarationCSS type."""


class TextIndexTypeError(TypeError):
    """Invalid type of the key."""


class TextInsertLineTypeError(TypeError):
    """Invalid value to insert to the text."""


class TextConcatenateLineTypeError(TypeError):
    """Invalid value to concatenate with the text."""


class AttributeBaseError(Exception):
    """Base class for Attribute errors to inherit."""


class AttributeValueTypeError(AttributeBaseError):
    """Attribute instance has an invalid value type that cannot be cast to the required one."""


class AttributeWithValueEmptyError(AttributeBaseError):
    """Attribute value cannot be None."""


class AttributeWithoutValueError(AttributeBaseError):
    """Attribute value must be None."""


class StylesheetBaseError(Exception):
    """Base class for Stylesheet errors to inherit."""


class StylesheetCSSWrongFileError(StylesheetBaseError):
    """File cannot be used as a CSS stylesheet."""


class DocImageShowError(AttributeError):
    """Attempt to open the DocImage instance failed."""


class GeneralFileInvalidError(ValueError):
    """File cannot be used as considered."""


class ListItemInvalidLevelError(ValueError):
    """Improper level value of the ListItem."""


class AttributeWithoutValueNotEmptyError(AttributeBaseError):
    """Attribute value cannot be set."""


class AttributeListNameNotFoundError(AttributeBaseError):
    """Attribute with such a name is not found."""


class AttributeListKeyTypeError(AttributeBaseError):
    """Attribute name has an invalid type."""


class ChoiseBaseError(Exception):
    """Base class for Choice errors to inherit."""


class ChoiceIndexNegativeError(ChoiseBaseError):
    """Choice value index must be a positive integer."""


class ChoiceIndexExceedsMaxError(ChoiseBaseError):
    """Choice value index must be less than the total number of choices."""


class ChoiceValueIsSpecifiedError(ChoiseBaseError):
    """Choice value has already been selected."""


class ChoiceValueError(ChoiseBaseError):
    """Choice value is not listed."""


class ChoiceIndexTypeError(ChoiseBaseError):
    """Choice index must be an integer."""


class StylesheetCSSSelectorNotFoundError(StylesheetBaseError):
    """Selector is not found in the CSS stylesheet file."""


class StylesheetItemTypeError(StylesheetBaseError):
    """Stylesheet item key has an invalid type."""
