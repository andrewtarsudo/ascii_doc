from functions import StrPath
from validated import GeneralFile


class Document(GeneralFile):
    def __init__(self, path: StrPath, **kwargs):
        super().__init__(path, **kwargs)

