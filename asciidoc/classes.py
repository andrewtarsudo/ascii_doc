from typing import Any, Iterable, NamedTuple

from loguru import logger

from exceptions import ChoiceIndexExceedsMaxError, ChoiceIndexNegativeError, ChoiceIndexTypeError, \
    ChoiceValueError, ChoiceValueIsSpecifiedError
from functions import IntNone, StrNone


class CellCoordinate(NamedTuple):
    row_index: IntNone
    column_index: IntNone

    def __str__(self):
        return f"({self.row_index}, {self.column_index})"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.row_index}, {self.column_index})>"

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return (self.row_index, self.column_index) == (other.row_index, other.column_index)
        else:
            return False

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return (self.row_index, self.column_index) != (other.row_index, other.column_index)
        else:
            return False

    def __lt__(self, other):
        if not isinstance(other, self.__class__):
            return NotImplemented
        if self.row_index == other.row_index:
            return self.column_index < other.column_index
        elif self.column_index == other.column_index:
            return self.row_index < other.row_index
        else:
            return False

    def __gt__(self, other):
        if not isinstance(other, self.__class__):
            return NotImplemented
        if self.row_index == other.row_index:
            return self.column_index > other.column_index
        elif self.column_index == other.column_index:
            return self.row_index > other.row_index
        else:
            return False

    def __le__(self, other):
        if not isinstance(other, self.__class__):
            return NotImplemented
        if self.row_index == other.row_index:
            return self.column_index <= other.column_index
        elif self.column_index == other.column_index:
            return self.row_index <= other.row_index
        else:
            return False

    def __ge__(self, other):
        if not isinstance(other, self.__class__):
            return NotImplemented
        if self.row_index == other.row_index:
            return self.column_index >= other.column_index
        elif self.column_index == other.column_index:
            return self.row_index >= other.row_index
        else:
            return False

    def __bool__(self):
        return self.row_index is not None and self.column_index is not None

    @classmethod
    def null_coordinate(cls):
        return cls(None, None)


class CellItem(NamedTuple):
    row_index: IntNone
    column_index: IntNone
    text: StrNone

    @property
    def coord(self) -> CellCoordinate:
        return CellCoordinate(self.row_index, self.column_index)

    def __str__(self):
        return f"{self.text}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.row_index}, {self.column_index}, {self.text})>"

    @property
    def is_null(self) -> bool:
        return self.coord.__bool__()

    def __bool__(self):
        return bool(self.text)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.coord == other.coord
        else:
            return False

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.coord != other.coord
        else:
            return False

    def __lt__(self, other):
        if isinstance(other, self.__class__):
            return self.coord < other.coord
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, self.__class__):
            return self.coord > other.coord
        else:
            return NotImplemented

    def __le__(self, other):
        if isinstance(other, self.__class__):
            return self.coord <= other.coord
        else:
            return NotImplemented

    def __ge__(self, other):
        if isinstance(other, self.__class__):
            return self.coord >= other.coord
        else:
            return NotImplemented

    def str_table_item(self):
        return f"|{self.text}"

    @classmethod
    def null_table_item(cls):
        return cls(None, None, None)


class Char(str):
    def __new__(cls, value: str):
        if len(value) != 1:
            logger.error(f"Char {value} must have a string of length 1 but {len(value)} received")
        _cls = super().__new__(cls)
        return str(value)


class Choice:
    def __init__(self, values: Iterable[Any] = None):
        if values is None:
            values: list[Any] = []
        self._values: tuple[Any] = (*values,)
        self._selected: IntNone = None

    def __iter__(self):
        return iter(self._values)

    def __bool__(self):
        return self._selected is not None

    def __len__(self):
        return len(self._values)

    def __getitem__(self, item):
        if isinstance(item, int):
            if item < 0:
                logger.error(f"Индекс значения {item} не может быть отрицательным числом")
                raise ChoiceIndexNegativeError
            elif item > len(self):
                logger.error(f"Индекс значения {item} не может превышать суммарное количество вариантов")
                raise ChoiceIndexExceedsMaxError
            else:
                return self._values[item]
        else:
            logger.error(f"Индекс {item} должен быть типа int, но получен {type(item)}")
            raise ChoiceIndexTypeError

    @property
    def choice(self):
        return self._selected

    @choice.setter
    def choice(self, value):
        if self._selected is not None:
            logger.error(f"Выбор {self._selected}, {self._values[self._selected]} уже сделан")
            raise ChoiceValueIsSpecifiedError
        if isinstance(value, int):
            if value < 0:
                logger.error(f"Индекс значения {value} не может быть отрицательным числом")
                raise ChoiceIndexNegativeError
            elif value > len(self):
                logger.error(f"Индекс значения {value} не может превышать суммарное количество вариантов")
                raise ChoiceIndexExceedsMaxError
            else:
                self._selected = value
        elif value in self._values:
            self._selected = self._values.index(value)
        else:
            logger.error(f"Выбор {value} невозможен")
            raise ChoiceValueError
