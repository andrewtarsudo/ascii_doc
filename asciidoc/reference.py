from loguru import logger

from validated import GeneralFile
from functions import StrPath, StrNone


class CrossReference(GeneralFile):
    def __init__(self, path: StrPath, element: StrNone, text: StrNone, **kwargs):
        suffix: str = ".adoc"
        self._element: StrNone = element
        self._text: StrNone = text
        super().__init__(path, suffix, **kwargs)

    def __str__(self):
        _element: str = f"#{self._element}" if self._element else ""
        _text: str = f",{self._text}" if self._text else ""
        return f"<<{self._path}{_element}{_text}>>"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path}, {self._element}, {self._text})>"

    def _validate(self):
        if not self._path.exists():
            logger.error(f"Файл {self._path} не найден")
            raise FileNotFoundError
        if not self._path.is_file():
            logger.error(f"Путь {self._path} указывает на директорию")
            raise IsADirectoryError
        return

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, value):
        self._path = value

    @property
    def element(self):
        return self._element

    @element.setter
    def element(self, value):
        self._element = value

    @element.deleter
    def element(self):
        self._element = None

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, value):
        self._text = value

    @text.deleter
    def text(self):
        self._text = None

    def __hash__(self):
        return hash((self._path, self._element))

    def __key(self):
        return self._path, self._element

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            if self.__key() == other.__key():
                return self._text == other._text
            return False
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            if self.__key() == other.__key():
                return self._text != other._text
            return True
        else:
            return NotImplemented
