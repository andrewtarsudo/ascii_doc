from typing import Any, Iterable

from admonition import AdmonitionType
from base_element import BaseElement
from attribute import Attribute
from const import BlockType
from functions import StrNone


class Block(BaseElement):
    def __bool__(self):
        return bool(self._text)

    def _validate(self):
        return

    def __init__(
            self,
            block_type: BlockType,
            label: StrNone = None,
            text: Iterable[str] = None,
            attributes: Iterable[Attribute] = None, **kwargs):
        super().__init__(attributes, **kwargs)
        if text is None:
            text: list[str] = []
        self._block_type: BlockType = block_type
        self._label: StrNone = label
        if isinstance(text, str):
            self._text: list[str] = text.split("\n")
        else:
            self._text: list[str] = [*text]

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._block_type}, {self._label}, {self.line}, )>"

    def _marker(self):
        return self._block_type.value

    def __str__(self):
        _: list[str] = [f"{k}={v}" for k, v in self._kwargs]
        _options: str = ",".join(_)
        _label: str = f".{self._label}\n" if self._label is not None else ""
        _text: str = self.line if self._text is not None else ""
        return f"{_label}[{_options}]\n{self._marker}\n{_text}\n{self._marker}"

    @property
    def line(self):
        return "\n".join(self._text) if self._text else ""

    @property
    def label(self):
        return self._label

    @label.setter
    def label(self, value):
        self._label = value

    @label.deleter
    def label(self):
        self._label = None

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, value):
        self._text = value

    @text.deleter
    def text(self):
        self._text = []

    @property
    def _kwargs(self) -> dict[str, Any]:
        return {arg: getattr(self, arg) for arg in self._args}

    def __len__(self):
        return len(self._text)

    def insert_text(self, text: str, index: int):
        self._text.insert(index % len(self), text)
        return

    def add_text(self, text: str):
        self._text[-1] = f"{self._text[-1]}{text}"
        return

    def __hash__(self):
        return hash((self._marker, self._label, self._text))

    def __key(self):
        return self._marker, self._label, self.line

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def iter_kwargs(self):
        return iter(self._kwargs.items())

    def iter_lines(self):
        return iter(self._text)


class AdmonitionBlock(Block):
    def __init__(
            self,
            label: str,
            admonition_type: AdmonitionType,
            text: Iterable[str] = None,
            attributes: Iterable[Attribute] = None,
            **kwargs):
        block_type: BlockType = BlockType.ADMONITION_BLOCK
        super().__init__(block_type, label, text, attributes, **kwargs)
        self._admonition_type: AdmonitionType = admonition_type

    def __str__(self):
        return f"[{self._admonition_type.name}]\n{self._marker}\n{self._text}\n{self._marker}"

    def __repr__(self):
        return f"<{self.__class__.__name__}(type={self._admonition_type.name},\ntext={self._text})>"


class BlockExample(Block):
    def __init__(
            self,
            label: str,
            text: Iterable[str] = None,
            attributes: Iterable[Attribute] = None, **kwargs):
        block_type: BlockType = BlockType.EXAMPLE_BLOCK
        super().__init__(block_type, label, text, attributes, **kwargs)


class BlockPassthrough(Block):
    def __init__(
            self,
            label: str,
            text: Iterable[str] = None,
            attributes: Iterable[Attribute] = None, **kwargs):
        block_type: BlockType = BlockType.PASSTHROUGH_BLOCK
        super().__init__(block_type, label, text, attributes, **kwargs)


class BlockOpen(Block):
    def __init__(
            self,
            label: str,
            text: Iterable[str] = None,
            attributes: Iterable[Attribute] = None, **kwargs):
        block_type: BlockType = BlockType.OPEN_BLOCK
        super().__init__(block_type, label, text, attributes, **kwargs)


class BlockSTEM(Block):
    def __init__(
            self,
            label: str,
            text: Iterable[str] = None,
            attributes: Iterable[Attribute] = None, **kwargs):
        block_type: BlockType = BlockType.STEM_BLOCK
        super().__init__(block_type, label, text, attributes, **kwargs)


class BlockComment(Block):
    def __init__(
            self,
            label: str,
            text: Iterable[str] = None,
            attributes: Iterable[Attribute] = None, **kwargs):
        block_type: BlockType = BlockType.COMMENT_BLOCK
        super().__init__(block_type, label, text, attributes, **kwargs)


class BlockFenced(Block):
    def __init__(
            self,
            label: str,
            text: Iterable[str] = None,
            attributes: Iterable[Attribute] = None, **kwargs):
        block_type: BlockType = BlockType.FENCED_BLOCK
        super().__init__(block_type, label, text, attributes, **kwargs)


class BlockListing(Block):
    def __init__(
            self,
            label: str,
            text: Iterable[str] = None,
            attributes: Iterable[Attribute] = None, **kwargs):
        block_type: BlockType = BlockType.LISTING_BLOCK
        super().__init__(block_type, label, text, attributes, **kwargs)


class BlockLiteral(Block):
    def __init__(
            self,
            label: str,
            text: Iterable[str] = None,
            attributes: Iterable[Attribute] = None, **kwargs):
        block_type: BlockType = BlockType.LITERAL_BLOCK
        super().__init__(block_type, label, text, attributes, **kwargs)


class BlockQuote(Block):
    def __init__(
            self,
            label: str,
            text: Iterable[str] = None,
            attributes: Iterable[Attribute] = None, **kwargs):
        block_type: BlockType = BlockType.QUOTE_BLOCK
        super().__init__(block_type, label, text, attributes, **kwargs)


class BlockSidebar(Block):
    def __init__(
            self,
            label: str,
            text: Iterable[str] = None,
            attributes: Iterable[Attribute] = None, **kwargs):
        block_type: BlockType = BlockType.SIDEBAR_BLOCK
        super().__init__(block_type, label, text, attributes, **kwargs)


class BlockSource(Block):
    def __init__(
            self,
            label: str,
            text: Iterable[str] = None,
            attributes: Iterable[Attribute] = None, **kwargs):
        block_type: BlockType = BlockType.SOURCE_BLOCK
        super().__init__(block_type, label, text, attributes, **kwargs)


class BlockVerse(Block):
    def __init__(
            self,
            label: str,
            text: Iterable[str] = None,
            attributes: Iterable[Attribute] = None, **kwargs):
        block_type: BlockType = BlockType.VERSE_BLOCK
        super().__init__(block_type, label, text, attributes, **kwargs)


class Table(Block):
    def __init__(
            self,
            label: str,
            text: Iterable[str] = None,
            attributes: Iterable[Attribute] = None, **kwargs):
        block_type: BlockType = BlockType.TABLE_BLOCK
        super().__init__(block_type, label, text, attributes, **kwargs)

    def set_header(self):
        if "header" not in self.iter_attributes():
            self.add_option("header")
