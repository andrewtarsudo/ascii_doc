from pathlib import Path

from loguru import logger

from exceptions import GeneralFileInvalidError
from functions import StrNone, StrPath


class Validated:
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._args: list[str] = [*kwargs.keys()]
        for k, v in kwargs.items():
            setattr(self, k, v)
        self._validate()

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.kwargs_output})>"

    def __str__(self):
        return f"{self.__class__.__name__}: {self.args_output}"

    @property
    def kwargs_output(self):
        _: list[str] = [f"{k}={v}" for k, v in self._kwargs]
        return ", ".join(_)

    @property
    def args_output(self):
        return ", ".join(self._args)

    def _validate(self):
        raise NotImplementedError

    @property
    def _kwargs(self):
        return {_: getattr(self, _) for _ in self._args}

    def get_arg(self, arg: str):
        return self._kwargs.get(arg, None)

    def __bool__(self):
        raise NotImplementedError

    @classmethod
    def from_string(cls, *args, **kwargs):
        raise NotImplementedError


class GeneralFile(Validated):
    @classmethod
    def from_string(cls, path: str, suffix: StrNone = None, **kwargs):
        return cls(path, suffix, **kwargs)

    def __init__(self, path: StrPath, suffix: StrNone = None, **kwargs):
        super().__init__(**kwargs)
        self._path: Path = Path(path).resolve()
        self._suffix: StrNone = suffix
        self._content: list[str] = []

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path}, {self.kwargs_output})>"

    def __str__(self):
        _: str = "" if not self.kwargs_output else f"\n{self.kwargs_output}"
        return f"{self.__class__.__name__}: {self._path}{_}"

    def _validate(self):
        _exists: bool = self._path.exists()
        _is_file: bool = self._path.is_file()
        if self._suffix is not None:
            _is_suffix: bool = self._path.suffix == self._suffix
        else:
            _is_suffix: bool = True

        if any(_ is False for _ in (_exists, _is_file, _is_suffix)):
            logger.error(f"Файл {self._path} должен иметь расширение {self._suffix}, но получено {self._path.suffix}")
            raise GeneralFileInvalidError
        return

    def read(self):
        with open(self._path, "r+") as f:
            _: list[str] = f.readlines()
        self._content = _

    def _content_str(self):
        return "\n".join(self._content)

    def write(self):
        with open(self._path, "w+") as f:
            f.write(self._content_str())

    def __iter__(self):
        return iter(self._content)

    def __bool__(self):
        return not self._content

    def __len__(self):
        return len(self._content)
