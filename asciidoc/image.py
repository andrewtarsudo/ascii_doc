from __future__ import annotations

from typing import NamedTuple

from PIL import Image
from loguru import logger

from exceptions import DocImageShowError
from validated import GeneralFile
from functions import StrPath, StrNone


class DocImage(GeneralFile):
    def __init__(self, title: StrNone, path: StrPath, **kwargs):
        self._title: StrNone = title
        super().__init__(path, **kwargs)

    def show(self):
        try:
            with Image.open(self._path) as image:
                image.show(self._title)
        except RuntimeError as e:
            logger.error(f"{e.__class__.__name__}, {str(e)}")
            raise
        except OSError as e:
            logger.error(f"{e.__class__.__name__}, {e.strerror}")
            raise

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path}, {self._title})>"

    def __str__(self):
        return f"{self.__class__.__name__}: {self._path}"

    def _validate(self):
        _exists: bool = self._path.exists()
        _is_file: bool = self._path.is_file()
        _is_css: bool = self._path.suffix == ".css"

        if any(_ is False for _ in (_exists, _is_file, _is_css)):
            logger.error(f"Файл {self._path} некорректен, поскольку не содержит стили CSS")
            raise DocImageShowError
        return

    @property
    def file(self):
        return self._path

    def image_link(self) -> ImageLink:
        return ImageLink(self._path, self._title)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._path == other._path
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._path != other._path
        else:
            return NotImplemented

    def __hash__(self):
        return hash(self._path)


class ImageLink(NamedTuple):
    link: StrPath
    text: StrNone

    def __str__(self):
        if self.text is None:
            return f"image::{self.link}[]"
        return f"image::{self.link}[{self.text}]"

    def __repr__(self):
        if self.text is None:
            return f"<{self.__class__.__name__}({self.link})>"
        return f"<{self.__class__.__name__}({self.link}, {self.text})>"
