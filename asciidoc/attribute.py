from abc import ABC
from typing import Any, Iterable

from loguru import logger

from const import AttributeType
from exceptions import AttributeListKeyTypeError, AttributeListNameNotFoundError, AttributeValueTypeError, \
    AttributeWithValueEmptyError, AttributeWithoutValueError, AttributeWithoutValueNotEmptyError
from functions import StrNone
from validated import Validated


class Attribute(Validated):
    @classmethod
    def from_string(cls, name: str, attribute_type_value: str, value: str, origin_type_value: StrNone, **kwargs):
        if origin_type_value is None:
            origin_type_value: str = "object"
        attribute_type: AttributeType = AttributeType.from_string(attribute_type_value)
        origin_type: type = object
        origin_type.__class__.__name__ = origin_type_value
        return cls(name, attribute_type, value=value, origin_type=origin_type, **kwargs)

    def __init__(
            self,
            name: str,
            attribute_type: AttributeType, *,
            value: Any = None,
            origin_type: type | None = None, **kwargs):
        self._name: str = name
        self._attribute_type: AttributeType = attribute_type
        if self._attribute_type.WITH_VALUE:
            if origin_type is None:
                origin_type: type = object
            self._value: Any = value
            self._origin_type: type = origin_type
        super().__init__(**kwargs)

    def __repr__(self):
        if self._attribute_type == AttributeType.WITH_VALUE:
            return f"<{self.__class__.__name__}({self._name}, {self._value}, {self._origin_type}, " \
                   f"{repr(self._attribute_type)})>"
        return f"<{self.__class__.__name__}({self._name}, {repr(self._attribute_type)})>"

    def __format__(self, format_spec):
        if format_spec == "doc":
            return self.str_doc()
        return self.str_element()

    def str_doc(self):
        if self:
            _: str = f" {self._value}"
        else:
            _: str = ""
        return f":{self._name}:{self._value}"

    def str_element(self):
        if self:
            return f'{self._name}="{self._value}"'
        else:
            return f"%{self._name}"

    def __str__(self):
        return self.str_element()

    def __key(self):
        return self._name, self._attribute_type

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() != other.__key()
        else:
            return NotImplemented

    def __hash__(self):
        return hash((self._name, self._attribute_type))

    def _validate_with_value(self):
        if self._value is None:
            logger.error(f"Значение атрибута {self._name} должно быть задано")
            raise AttributeWithValueEmptyError
        try:
            self._value.__class__ = self._origin_type
        except (TypeError | ValueError | OSError | RuntimeError) as e:
            logger.error(f"Атрибут {self._name} со значением {self._value} не может быть преобразован к типу "
                         f"{self._origin_type}")
            logger.error(f"{e.__class__.__name__}, {str(e)}")
            raise AttributeValueTypeError
        return

    def _validate_without_value(self):
        if self._value is not None:
            logger.error(f"Значение атрибута {self._name} не может быть задано")
            raise AttributeWithoutValueNotEmptyError
        return

    def _validate(self):
        return self._validate_with_value() if self else self._validate_without_value()

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, _value: Any):
        if not self:
            logger.error(f"Значение атрибута {self._name} не может быть задано")
            return
        if _value is None:
            logger.error(f"Значение атрибута {self._name} должно быть задано")
            return
        self._value = _value

    @value.deleter
    def value(self):
        self._value = None

    @property
    def name(self):
        return self._name

    @property
    def attribute_type(self):
        return self._attribute_type

    def __bool__(self):
        return self._attribute_type == AttributeType.WITH_VALUE

    @property
    def origin_type(self):
        return self._origin_type


class AttributeList(ABC):
    def __init__(self, attributes: Iterable[Attribute] = None):
        if attributes is None:
            attributes: set[Attribute] = set()
        self._attributes: set[Attribute] = set(attributes)

    def _attributes_repr(self) -> str:
        return "\n".join([repr(_) for _ in iter(self)])

    def __repr__(self):
        return f"<{self.__class__.__name__}(\n{self._attributes_repr()}\n)>"

    def __str__(self):
        return "\n".join([str(_) for _ in iter(self)])

    def __iter__(self):
        return iter(self._attributes)

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self.attribute_names
        elif isinstance(item, Attribute):
            return item in self._attributes
        else:
            return False

    def __len__(self):
        return len(self._attributes)

    def __setitem__(self, key, value):
        if not isinstance(key, str):
            logger.error(f"Ключ {key} должен быть типа str, но получен {type(key)}")
            raise AttributeListKeyTypeError
        if key in self.attribute_names:
            if self[key].attribute_type == AttributeType.WITH_VALUE and self[key].origin_type is not type(value):
                logger.error(f"Значение {value} должно быть типа {self[key].origin_type}, но получено {type(value)}")
                raise AttributeValueTypeError
            elif self[key].attribute_type == AttributeType.WITHOUT_VALUE and value is not None:
                logger.error(f"Значение не может быть определено для ключа {key}")
                raise AttributeWithoutValueError
        attribute_type: AttributeType = AttributeType.WITHOUT_VALUE if value is None else AttributeType.WITH_VALUE
        attribute: Attribute = Attribute(key, attribute_type, value=value, origin_type=type(value))
        self._attributes.add(attribute)

    def __getitem__(self, item):
        if not isinstance(item, str):
            logger.error(f"Ключ {item} должен быть типа str, но получен {type(item)}")
            raise AttributeListKeyTypeError
        if item not in self.attribute_names:
            logger.error(f"Атрибут с именем {item} не найден")
            raise AttributeListNameNotFoundError
        for attribute in iter(self):
            if attribute.name == item:
                return attribute

    @property
    def attribute_names(self) -> list[str]:
        return [_.name for _ in iter(self)]

    def __add__(self, other: Any):
        if not isinstance(other, Attribute):
            return NotImplemented
        if other.name in self.attribute_names:
            return logger.error(f"Атрибут {other.name} уже в списке")
        logger.debug(f"Добавлен атрибут {other}")
        return self._attributes.add(other)

    __iadd__ = __add__
    __radd__ = __add__

    def __sub__(self, other):
        if isinstance(other, Attribute):
            logger.debug(f"Удален атрибут {other.name}")
            return self._attributes.discard(other)
        elif isinstance(other, str):
            if other in self.attribute_names:
                logger.debug(f"Удален атрибут {other}")
                return self._attributes.discard(self[other])
            else:
                return logger.debug(f"Атрибут {other} уже удален")
        else:
            return NotImplemented


def attribute_factory(name: str, value: Any = None, **kwargs):
    if value is None:
        attribute_type: AttributeType = AttributeType.WITHOUT_VALUE
        origin_type: type | None = None
    else:
        attribute_type: AttributeType = AttributeType.WITHOUT_VALUE
        origin_type: type | None = type(value)
    return Attribute(name, attribute_type, value=value, origin_type=origin_type, **kwargs)
