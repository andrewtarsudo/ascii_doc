from typing import Iterable, Iterator, NamedTuple

from loguru import logger

from asciidoc.base_element import BaseElement
from asciidoc.exceptions import DeclarationCSSEqualPropertyError, ElementCSSDeleteItemError, ElementCSSNewItemError


class DeclarationCSS(NamedTuple):
    """Specifies the CSS declaration.
    """
    parameter: str
    value: str | Iterable[str]
    is_multiple: bool = False
    comment: str = None

    def __str__(self):
        _value: str = ", ".join(*self.value) if self.is_multiple else self.value
        if not bool(self):
            return f"{self.parameter}: {_value}; /* {self.comment} */"
        else:
            return f"{self.parameter}: {_value};"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.parameter}, {self.value}, {self.is_multiple}, {self.comment})>"

    def __hash__(self):
        return hash(self.parameter)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.parameter == other.parameter
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.parameter != other.parameter
        else:
            return NotImplemented

    def __bool__(self):
        return self.comment is None


class ElementCSS(BaseElement):
    def __init__(self, selectors: Iterable[str] = None, declarations: Iterable[DeclarationCSS] = None, **kwargs):
        if selectors is None:
            selectors: list[str] = []
        if declarations is None:
            declarations: list[DeclarationCSS] = []
        self._selectors: list[str] = [*selectors]
        self._declarations: list[DeclarationCSS] = [*declarations]
        super().__init__(**kwargs)

    def _str_selectors(self, separator: str = ", ") -> str:
        return separator.join(self._selectors)

    def _str_declarations(self, separator: str = "\n") -> str:
        return separator.join([str(_) for _ in self._declarations])

    def __str__(self):
        return "%s {%s\n}" % (self._str_selectors(), self._str_declarations("  \n"))

    def __repr__(self):
        return f"<{self.__class__.__name__}>\n{self._str_selectors()}\n{self._str_declarations()}"

    def __len__(self):
        return len(self._selectors)

    def __iter__(self) -> Iterator[DeclarationCSS]:
        return iter(self._declarations)

    def len_declarations(self) -> int:
        return len(self._declarations)

    def parameters(self) -> list[str]:
        return [_.parameter for _ in iter(self)]

    def iter_selectors(self) -> Iterator[str]:
        return iter(self._selectors)

    def _validate(self):
        if len(set(self.parameters())) != self.len_declarations():
            logger.error(f"Хотя бы два объявления используют одинаковое свойство")
            raise DeclarationCSSEqualPropertyError
        return

    def __hash__(self):
        return hash(self._selectors)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._selectors == other._selectors
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._selectors != other._selectors
        else:
            return NotImplemented

    def __bool__(self):
        return not (self._selectors or self._declarations)

    def __add__(self, other):
        if isinstance(other, str):
            if other in self._selectors:
                logger.info(f"Селектор {other} уже указан")
            else:
                self._selectors.append(other)
                logger.debug(f"Селектор {other} добавлен")
        elif isinstance(other, DeclarationCSS):
            if other.parameter in self.parameters():
                logger.info(f"Объявление {other} использует свойство, которое уже указано")
            else:
                self._declarations.append(other)
                logger.debug(f"Объявление {other} добавлено")
        else:
            logger.error(f"Ожидалось значение типа str или DeclarationCSS, однако получено {type(other)}")
            raise ElementCSSNewItemError

    __iadd__ = __add__
    __radd__ = __add__

    @property
    def selectors(self) -> list[str]:
        return self._selectors

    def __sub__(self, other):
        if isinstance(other, str):
            if other in self._selectors:
                self._selectors.remove(other)
                logger.debug(f"Селектор {other} удален")
            else:
                logger.info(f"Селектор {other} не найден")
        elif isinstance(other, DeclarationCSS):
            if other.parameter in self.parameters():
                self._declarations.remove(other)
                logger.debug(f"Объявление {other} удалено")
            else:
                logger.info(f"Объявление {other} не найдено")
        else:
            logger.error(f"Ожидалось значение типа str или DeclarationCSS, однако получено {type(other)}")
            raise ElementCSSDeleteItemError
