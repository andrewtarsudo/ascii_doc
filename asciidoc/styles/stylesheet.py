from pathlib import Path
from typing import Iterable, Iterator

from loguru import logger

from asciidoc.styles.style import ElementCSS
from asciidoc.exceptions import StylesheetCSSSelectorNotFoundError, StylesheetCSSWrongFileError, StylesheetItemTypeError
from asciidoc.validated import GeneralFile
from functions import StrPath


class StylesheetCSS(GeneralFile):
    def __init__(self, file: StrPath, **kwargs):
        self._file: Path = Path(file).resolve()
        self._elements: list[ElementCSS] = []
        super().__init__(**kwargs)

    def _validate(self):
        _exists: bool = self._file.exists()
        _is_file: bool = self._file.is_file()
        _is_css: bool = self._file.suffix == ".css"

        if any(_ is False for _ in (_exists, _is_file, _is_css)):
            logger.error(f"Файл {self._file} некорректен, поскольку не содержит стили CSS")
            raise StylesheetCSSWrongFileError
        return

    def __str__(self):
        return f"{self.__class__.__name__}: {self._file}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._file}, )>"

    def __iter__(self) -> Iterator[ElementCSS]:
        return iter(self._elements)

    def __len__(self):
        return len(self._elements)

    def selectors(self) -> list[str]:
        return [s for _ in iter(self) for s in _.selectors]

    def _find_element_css(self, selector: str) -> ElementCSS:
        element: ElementCSS
        for element in iter(self):
            if selector in element.selectors:
                return element
        logger.error(f"Селектор {selector} не найден среди элементов")
        raise StylesheetCSSSelectorNotFoundError

    def __getitem__(self, item):
        if isinstance(item, (int, slice)):
            return self._elements[item]
        elif isinstance(item, str):
            return self._find_element_css(item)
        else:
            logger.error(f"Ключ {item} должен быть типа int, slice или str, но получен {type(item)}")
            raise StylesheetItemTypeError

    def __add__(self, other):
        if isinstance(other, ElementCSS):
            self._elements.append(other)
            self._content.append(str(other))
            self.write()
        elif isinstance(other, Iterable):
            _new_elements_css = list(filter(lambda x: isinstance(x, ElementCSS), other))
            self._elements.extend(_new_elements_css)
            self._content.extend([str(element_css) for element_css in _new_elements_css])
            self.write()
        else:
            return NotImplemented

    __radd__ = __add__
    __iadd__ = __add__

    def __sub__(self, other):
        if not isinstance(other, ElementCSS):
            return NotImplemented
        if other in self._elements:
            return self._elements.remove(other)
        logger.info(f"Элемент {other} не найден")
        return

    def __hash__(self):
        return hash(self._file)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._file == other._file
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._file != other._file
        else:
            return NotImplemented

    def __contains__(self, item):
        if isinstance(item, ElementCSS):
            return item in self._elements
        elif isinstance(item, str):
            return item in self.selectors()
        else:
            return False

    def __bool__(self):
        return len(self) != 0
