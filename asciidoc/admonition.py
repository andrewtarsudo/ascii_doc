from asciidoc.const import AdmonitionType
from asciidoc.validated import Validated
from functions import StrNone


class Admonition(Validated):
    @classmethod
    def from_string(cls, admonition_type_value: str, text: StrNone, **kwargs):
        admonition_type: AdmonitionType = AdmonitionType.from_string(admonition_type_value)
        return cls(admonition_type, text, False, **kwargs)

    def _validate(self):
        pass

    def __bool__(self):
        return bool(self._text)

    def __init__(self, admonition_type: AdmonitionType, text: StrNone, is_block: bool = False, **kwargs):
        super().__init__(**kwargs)
        self._admonition_type: str = admonition_type.value
        if text is None:
            text: str = ""
        self._text: str = text
        self._is_block: bool = is_block

    def __str__(self):
        if self._is_block:
            return f"[{self._admonition_type}]\n----\n{self._text}\n----"
        else:
            return f"{self._admonition_type}: {self._text}"

    def __repr__(self):
        return f"<{self.__class__.__name__}(type={self._admonition_type},\ntext={self._text}," \
               f"\nblock={self._is_block})>"

    @property
    def block(self):
        return self._is_block

    @block.setter
    def block(self, value):
        self._is_block = value

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, value):
        self._text = value
