from enum import Enum

from loguru import logger


class BlockType(Enum):
    ADMONITION_BLOCK = "----"
    PASSTHROUGH_BLOCK = "++++"
    COMMENT_BLOCK = "////"
    EXAMPLE_BLOCK = "===="
    LISTING_BLOCK = "----"
    LITERAL_BLOCK = "...."
    OPEN_BLOCK = "--"
    SIDEBAR_BLOCK = "****"
    QUOTE_BLOCK = "____"
    SOURCE_BLOCK = "----"
    STEM_BLOCK = "++++"
    FENCED_BLOCK = "```"
    VERSE_BLOCK = "____"
    TABLE_BLOCK = "|---"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._name_})>"

    def __str__(self):
        return f"{self._value_}"


class AdmonitionType(Enum):
    CAUTION = "CAUTION"
    IMPORTANT = "IMPORTANT"
    NOTE = "NOTE"
    TIP = "TIP"
    WARNING = "WARNING"

    def __repr__(self):
        return f"<{self.__class__.__name__}[{self._name_}]>"

    def __str__(self):
        return f"{self.__class__.__name__}[{self._name_}]"

    def __iter__(self):
        return iter(super().__class__)

    def __missing__(self, key):
        _values: str = ", ".join([_.value for _ in iter(self)])
        logger.error(f"Специальный блок типа {key} не найден. Возможные значения: {_values}.")
        return

    @classmethod
    def _missing_(cls, value: object):
        _values: str = ", ".join(cls._member_map_.keys())
        logger.error(f"Специальный блок типа {value} не найден. Возможные значения: {_values}.")
        return

    @classmethod
    def from_string(cls, value: str):
        if value in cls._member_map_.keys():
            return cls[value]
        return cls._missing_(value)


class AttributeType(Enum):
    WITH_VALUE = "with_value"
    WITHOUT_VALUE = "without_value"

    def __str__(self):
        return f"{self.__class__.__name__}[{self._name_}]"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._name_}, {self._value_})>"

    def __iter__(self):
        return iter(super().__class__)

    def __missing__(self, key):
        _values: str = ", ".join([_.value for _ in iter(self)])
        logger.error(f"Специальный блок типа {key} не найден. Возможные значения: {_values}.")
        return

    @classmethod
    def _missing_(cls, value: object):
        _values: str = ", ".join(cls._member_map_.keys())
        logger.error(f"Специальный блок типа {value} не найден. Возможные значения: {_values}.")
        return

    @classmethod
    def from_string(cls, value: str):
        if value in cls._member_map_.keys():
            return cls[value]
        return cls._missing_(value)
