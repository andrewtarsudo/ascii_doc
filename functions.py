from pathlib import Path
from typing import Iterable, Sequence, Sized, TypeAlias

from loguru import logger

from asciidoc.exceptions import ItemsEmptyError, AsciiDocLineNotFoundError

StrPath: TypeAlias = str | Path
StrNone: TypeAlias = str | None
IntNone: TypeAlias = int | None


def get_path(path: StrPath) -> Path:
    if isinstance(path, str):
        path: Path = Path(path)
    return path.resolve()


def get_key(key: int, item: Sized) -> int:
    return 0 if len(item) == 0 else key % len(item)


def min_max(limit: int, items: Iterable[int] = None) -> tuple[int, int]:
    if items is None:
        logger.error("Перечень пуст")
        raise ItemsEmptyError
    if limit == 0:
        logger.error(f"Ограничение {limit} не может быть 0")
        raise ZeroDivisionError
    _: tuple[int, ...] = tuple(item % abs(limit) for item in items)
    return min(_), max(_)


def get_line_index(item: str | int, values: Sequence[str] = None) -> tuple[int, str] | None:
    if values is None:
        logger.error(f"Перечень пуст")
        raise ItemsEmptyError
    if isinstance(item, str):
        if item not in values:
            logger.error(f"Строка {item} не найден")
            raise AsciiDocLineNotFoundError
        return [*values].index(item), item
    elif isinstance(item, int):
        item %= len(values)
        return item, values[item]
    else:
        return


def non_empty(start: int = 0, values: Iterable[str] = None):
    _: int = start
    for index, line in enumerate(iter(values[start:])):
        if line:
            _ += index
    return _
