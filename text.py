from typing import Iterable, Any

from loguru import logger

from asciidoc.exceptions import TextInsertLineTypeError, TextConcatenateLineTypeError
from functions import StrNone
from asciidoc._validated import Validated


class Text(Validated):
    def __init__(self, line: StrNone = None, *, lines: Iterable[str] = None, **kwargs):
        super().__init__(**kwargs)
        if line is None and lines is None:
            self._line: str = ""
            self._lines: list[str] = [""]
        elif line is None:
            self._line: str = "\n".join(lines)
            self._lines: list[str] = [*lines]
        elif lines is None:
            self._line: str = line
            self._lines: list[str] = [_ for _ in line.split() if _]

    def _validate(self):
        return

    def __str__(self):
        return self._line

    def __repr__(self):
        _lines: str = "\n".join(self._lines)
        return f"<{self.__class__.__name__}(\nline = {self._line},\nlines = {_lines})>"

    def __len__(self):
        return len(self._lines)

    def __add__(self, other):
        if isinstance(other, self.__class__):
            self._lines.extend(other._lines)
            self._line = f"{self._line}\n{other._line}"
            return
        elif isinstance(other, str):
            self._lines.append(other)
            self._line = f"{self._line}\n{other}"
            return
        elif isinstance(other, Iterable):
            return self.add_lines(other)
        else:
            return NotImplemented

    __radd__ = __add__
    __iadd__ = __add__

    def __getitem__(self, item):
        if isinstance(item, (int, slice)):
            return self._lines[item]
        else:
            logger.error(f"Некорректный тип ключа {type(item)} для {item}")
            raise

    def __hash__(self):
        return hash(self._line)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._line == other._line
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._line != other._line
        else:
            return NotImplemented

    def __le__(self, other):
        if isinstance(other, self.__class__):
            return self._line <= other._line
        else:
            return NotImplemented

    def __ge__(self, other):
        if isinstance(other, self.__class__):
            return self._line >= other._line
        else:
            return NotImplemented

    def __lt__(self, other):
        if isinstance(other, self.__class__):
            return self._line < other._line
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, self.__class__):
            return self._line > other._line
        else:
            return NotImplemented

    def add_lines(self, lines: Iterable[Any]):
        _new_lines: list[str] = []
        for line in lines:
            if isinstance(line, self.__class__):
                _new_lines.extend(line._lines)
            elif isinstance(line, str):
                _new_lines.append(line)
            else:
                continue
        self._lines.extend(_new_lines)
        _: str = "\n".join(_new_lines)
        self._line = f"{self._line}\n{_}"
        return

    def __format__(self, format_spec):
        if format_spec == "single":
            return self._line
        elif format_spec == "multiple":
            return self._lines
        else:
            return repr(self)

    def insert_line(self, index: int, line: Any):
        if index < 0:
            index = -index
        if isinstance(line, str):
            pass
        elif isinstance(line, self.__class__):
            line = line._line
        else:
            logger.error(f"Строка {line} должна быть str или {self.__class__}, но получено {type(line)}")
            raise TextInsertLineTypeError
        self._lines.insert(index, line)
        self._line = "\n".join(self._lines)
        return

    def concatenate(self, line: Any):
        if isinstance(line, str):
            pass
        elif isinstance(line, self.__class__):
            line = line._line
        else:
            logger.error(f"Строка {line} должна быть str или {self.__class__}, но получено {type(line)}")
            raise TextConcatenateLineTypeError
        self._line = f"{self._line}{line}"
        self._lines[-1] = f"{self._lines[-1]}{line}"
        return
