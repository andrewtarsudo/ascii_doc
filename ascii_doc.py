from __future__ import annotations

from pathlib import Path
from re import fullmatch, compile, Pattern
from typing import Any, Iterable, Iterator

from loguru import logger

from asciidoc.exceptions import AsciiDocLineInitError, AsciiDocTableNotationError, DocumentLineKeyTypeError, \
    DocumentPartValueTypeError
from functions import StrPath, get_key, get_line_index, get_path, min_max


class AsciiDocLine:
    def __init__(self, item: str | int, parent: Any, **kwargs):
        if not isinstance(parent, AsciiDoc):
            logger.error(f"Атрибут item, {item}, имеет некорректный тип {type(item)}")
            raise AsciiDocLineInitError
        index, line = get_line_index(item, parent.lines)
        self._line: str = line
        self._index: int = index
        self.parent = parent
        if kwargs is not None:
            for k, v in kwargs.items():
                setattr(self, k, v)
        if "separator" not in kwargs.keys:
            self.separator: str = "| "

    def add_to_element(self, parent: Any):
        if self.parent is None:
            self.parent = parent

    def remove_from_element(self):
        self.parent = None

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._line})>"

    def replace_to(self, index: int):
        if index < 0:
            logger.info("Некорректный индекс строки")
            return
        self._index = index

    def get_up(self, diff: int):
        if self._index + diff < 0:
            logger.info("Некорректный индекс строки")
            return
        self._index += diff

    def __key(self):
        return self._index, self._line

    def __hash__(self):
        return hash((self._index, self._line))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            if self.parent == other.parent:
                return self.__key() == other.__key()
            return False
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            if self.parent == other.parent:
                return self.__key() != other.__key()
            return True
        else:
            return NotImplemented

    def __lt__(self, other):
        if isinstance(other, self.__class__) and self.parent == other.parent:
            return self._index < other._index
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, self.__class__) and self.parent == other.parent:
            return self._index > other._index
        else:
            return NotImplemented

    def __le__(self, other):
        if isinstance(other, self.__class__):
            if self.parent != other.parent:
                return False
            return self._index <= other._index
        else:
            return NotImplemented

    def __ge__(self, other):
        if isinstance(other, self.__class__):
            if self.parent != other.parent:
                return False
            return self._index >= other._index
        else:
            return NotImplemented

    def parse(self) -> list[str]:
        return list(iter(self))

    def __iter__(self) -> Iterator[str]:
        return iter(_.strip() for _ in self._line.split(self.separator) if _.strip())


class AsciiDocFile:
    def __init__(self, path: StrPath):
        self._path: Path = get_path(path)
        self._content: list[str] = []

    def read(self):
        with open(self._path, "r+") as f:
            _: list[str] = f.readlines()
        self._content = _

    def write(self, content: Iterable[str] = None):
        if content is None:
            content: list[str] = self._content
        with open(self._path, "w+") as f:
            f.write("\n".join(content))

    def get_ascii_doc(self):
        return AsciiDoc(self._content)

    def __str__(self):
        return "\n".join(self._content)


class AsciiDoc:
    def __init__(self, lines: Iterable[str] = None):
        if lines is None:
            lines: list[str] = []
        self._lines: list[str] = [*lines]
        self.parts: dict[str, list[AsciiDocPart]] = dict()

    def __str__(self):
        return "\n".join(self._lines)

    def __iter__(self):
        return iter(self.ascii_lines)

    def __contains__(self, item):
        return item in self.ascii_lines

    def __bool__(self):
        return len(self) != 0

    def _set_key(self, key: int):
        return get_key(key, self)

    @property
    def ascii_lines(self):
        return [AsciiDocLine(line, self) for line in iter(self._lines)]

    def __getitem__(self, item):
        if isinstance(item, (int, slice)):
            return self._lines[item]
        elif isinstance(item, str):
            return self.parts.get(item)
        else:
            logger.info(f"Некорректный ключ {item}: должен быть int, slice или str, но получен {type(item)}")
            raise DocumentLineKeyTypeError

    def __setitem__(self, key, value):
        if isinstance(key, str):
            if not isinstance(value, AsciiDocPart):
                logger.info(f"Значение {str(value)} должно быть AsciiDocPart, но получено {type(value)}")
                raise DocumentPartValueTypeError
            if value in self.ascii_doc_parts():
                logger.info(f"Часть {str(value)} уже записана")
                return
            if key not in self.parts.keys():
                self.parts[key] = []
            self.parts[key].append(value)
        elif isinstance(key, int):
            if not isinstance(value, str):
                logger.info(f"Значение {str(value)} должно быть str, но получено {type(value)}")
                raise DocumentPartValueTypeError
            self._lines[self._set_key(key)] = value
        else:
            logger.info(f"Некорректный ключ {key}: должен быть int или str, но получен {type(key)}")
            raise DocumentLineKeyTypeError

    def __len__(self):
        return len(self._lines)

    def insert_line(self, line: str, index: int = None):
        index: int = self._set_key(index)
        return self._lines.insert(index, line)

    def remove_line(self, key):
        if isinstance(key, int):
            key = self._set_key(key)
            return self._lines.__delitem__(key)
        elif isinstance(key, str):
            if key in self.ascii_lines:
                return self._lines.remove(key)
            else:
                logger.info(f"Строка {key} не найдена")
                return
        else:
            logger.info(f"Некорректный ключ {key}: должен быть int или str, но получен {type(key)}")
            raise DocumentLineKeyTypeError

    def iter_parts(self):
        return iter(_ for value in self.parts.values() for _ in value)

    def part_names(self):
        return self.parts.keys()

    def ascii_doc_parts(self):
        return list(self.iter_parts())

    @property
    def lines(self):
        return self._lines



class AsciiDocPart:
    def __init__(self, name: str, ascii_doc: AsciiDoc, lines: Iterable[str] = None):
        if lines is None:
            lines: list[str] = []
        self._name: str = name
        self._ascii_doc: AsciiDoc = ascii_doc
        self._lines: list[str] = [*lines]

        if self._name not in self._ascii_doc.parts.keys():
            self._ascii_doc.parts[self._name] = []
        self._ascii_doc.parts[self._name].append(self)
        self._index: int = len(self._ascii_doc.parts[self._name]) - 1

    def __iter__(self):
        return iter(self._lines)

    def __contains__(self, item):
        return item in self._lines

    def __str__(self):
        return "\n".join(self._lines)

    def __hash__(self):
        return hash(self._lines)

    def __len__(self):
        return len(self._lines)

    def __getitem__(self, item):
        return self._lines[item]

    def __setitem__(self, key, value):
        self._lines[key] = value

    def min_max(self, items: Iterable[int] = None):
        return min_max(len(self), items)

    def ascii_doc_lines(self, **kwargs) -> list[AsciiDocLine]:
        return list(self.iter_ascii_doc_lines(**kwargs))

    def iter_ascii_doc_lines(self, **kwargs) -> Iterator[AsciiDocLine]:
        return iter(
            AsciiDocLine(line, self._ascii_doc.parts[self._name][self._index], **kwargs)
            for line in iter(self))


class AsciiDocTable(AsciiDocPart):
    def __init__(self, ascii_doc: AsciiDoc, start: int, stop: int):
        name: str = "table"
        _start, _stop = self.min_max((start, stop))
        lines: list[str] = ascii_doc[_start:_stop + 1]
        if not lines[0].startswith("|===") or not lines[-1].startswith("|==="):
            logger.error(f"Start {lines[0]} and last {lines[-1]} lines are invalid")
            raise AsciiDocTableNotationError
        super().__init__(name, ascii_doc, lines)

    @property
    def _header_index(self):
        pattern: Pattern = compile(r"(\|([\w\s\-]*))*")
        _index: int = 2
        for index, line in enumerate(iter(self._lines[2:])):
            if fullmatch(pattern, line):
                _index += index
                break
        return _index

    @property
    def _header(self):
        return AsciiDocLine(self._lines[self._header_index], self)

    @property
    def columns(self):
        return self._header.parse()

    def content(self):
        return get_line_index(self._header_index, self._lines)
