from __future__ import annotations

from typing import Iterable, Iterator

from loguru import logger

from asciidoc.classes import CellCoordinate, CellItem
from functions import StrPath


class FileTable:
    def __init__(self, items: Iterable[CellItem] = None):
        if items is None:
            items: list[CellItem] = []
        self._items: list[CellItem] = [*items]

    @classmethod
    def from_file(cls, lines: Iterable[str] = None):
        if lines is None:
            lines: list[str] = []
        _items: list[CellItem] = []
        for row_index, line in enumerate(iter(lines)):
            for column_index, content in enumerate(line.strip("|").split(" | ")):
                _value: CellItem = CellItem(row_index, column_index, content.strip(""))
        return cls(_items)

    def write(self, path: StrPath):
        with open(path, "w+") as f:
            f.writelines(str(self))

    def __str__(self):
        return "\n".join(str(item) for item in iter(self))

    def __iter__(self) -> Iterator[CellItem]:
        return iter(self._items)

    def _coords(self):
        return [_.coord for _ in iter(self)]

    def _find_by_coord(self, coord: CellCoordinate):
        if coord not in self._coords():
            logger.error("Key is not found")
        for _ in iter(self):
            if _.coord == coord:
                return _
        else:
            return

    def __getitem__(self, item):
        if isinstance(item, (int, slice)):
            return self._items[item]
        elif isinstance(item, CellCoordinate):
            return self._find_by_coord(item)
        else:
            raise KeyError

    def __setitem__(self, key, value):
        if isinstance(key, int):
            if isinstance(value, str):
                self._lines.__setitem__(key, value)
            else:
                ...
        elif isinstance(key, CellCoordinate):
            if isinstance(value, CellItem):
                self._items.__setitem__(key, value)
            else:
                ...
        else:
            ...

    def __len__(self):
        return len(self._items)

    def __hash__(self):
        return hash(self._items)

    def __add__(self, other):
        if isinstance(other, FileTableRow):
            self._lines.append(other.__str__())
            other.to_markdown_table(self, self.__len__())
        elif isinstance(other, str):
            self._lines.append(other)
        else:
            return NotImplemented

    __radd__ = __add__
    __iadd__ = __add__

    def insert_row(self, row: FileTableRow | str, index: int = -1):
        if index == -1:
            return self.__add__(row)
        if isinstance(row, FileTableRow):
            return self._lines.insert(index, row.__str__())
        elif isinstance(row, str):
            return self._lines.insert(index, row)
        return

    def iter_rows(self) -> Iterator[FileTableRow]:
        return iter(FileTableRow(line, self, index) for index, line in enumerate(self.__iter__()))

    def rows(self) -> list[FileTableRow]:
        return list(self.iter_rows())

    def get_row(self, item):
        return self.rows()[item]

    def get_line(self, row_index: int):
        return [_.text for _ in iter(self) if _.coord.row_index == row_index]

    # def replace_row(self, __from: int, __to: int):
    #     _row: str = self._lines.pop(__from)
    #     self.insert_row(_row, __to)

    # def delete_row(self, index: int):
    #     self._lines.pop(index)

    @property
    def items(self):
        return self._items

    def text(self):
        return [_.text for _ in iter(self)]

    @property
    def lines(self):
        return [self.get_line(row_index) for row_index in range(self.max_row)]

    @property
    def max_row(self):
        return max(_.coord.row_index for _ in iter(self))

    @property
    def max_column(self):
        return max(_.coord.column_index for _ in iter(self))


class FileTableRow:
    def __init__(self, line: str, markdown_table: FileTable | None = None, row_index: int | None = None):
        self._line: str = line
        self._markdown_table: FileTable | None = markdown_table
        self._row_index: int | None = row_index
        self._cells: list[CellItem] = []

    def to_markdown_table(self, markdown_table: FileTable, row_index: int):
        self._markdown_table = markdown_table
        self._row_index = row_index

    def __str__(self):
        return f"| {' | '.join(self._cells)} |"

    def __iter__(self):
        return iter(self._cells)

    def __getitem__(self, item):
        return self._cells.__getitem__(item)

    def __setitem__(self, key, value):
        self._cells.__setitem__(key, value)

    def __len__(self):
        return len(self._cells)

    def __bool__(self):
        return self.__len__() == 1

    def up_down(self, diff: int = 0):
        if self._markdown_table is not None:
            self._row_index += diff

    def update(self):
        self._line = self.__str__()
        if self._markdown_table is not None:
            self._markdown_table.__setitem__(self._row_index, self._line)

    def insert_cell(self, cell: str, index: int = -1):
        if index == -1:
            self._cells.append(cell)
        else:
            self._cells.insert(index, cell)

    def replace_cell(self, __from: int, __to: int):
        _row: str = self._cells.pop(__from)
        self._cells.insert(__to, _row)

    def delete_cell(self, index: int):
        self._cells.pop(index)

    def split_cell(self, index: int, *, text_old: str | None = None, text_new: str | None = None):
        if text_old is None:
            text_old: str = self.__getitem__(index)
        if text_new is None:
            text_new: str = ""
        self.insert_cell(text_new, index + 1)
        self.__setitem__(index, text_old)

    is_subheader = __bool__
